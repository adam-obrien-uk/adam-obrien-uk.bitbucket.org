(function (angular) {
  angular.module('SimpleServer', ['ngRoute', 'ui.bootstrap']);
})(angular);

(function (angular) {
  angular.module('SimpleServer').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $locationProvider.hashPrefix('');

    $routeProvider
      .when('/home', {
        controller: 'HomeController',
        templateUrl: './app/site/home/index.html',
      })
      .when('/test', {
        controller: 'TestController',
        templateUrl: './app/site/test/index.html'
      })
      .when('/wishlist_old', {
        controller: 'WishlistControllerOld',
        templateUrl: './app/site/wishlist/index_old.html'
      })
      .when('/wishlist', {
        controller: 'WishlistController',
        templateUrl: './app/site/wishlist/index.html'
      })

      .when('/shop', {
        controller: 'ShopController',
        templateUrl: './app/site/shop/index.html'
      })
      .when('/shop/items', {
        controller: 'ShopController',
        templateUrl: './app/site/shop/items.html'
      })
      .when('/shop/item/:idx', {
        controller: 'ShopController',
        templateUrl: './app/site/shop/item.html'
      })
      .otherwise({ redirectTo: '/shop/items' });

  }]);
})(angular);

(function (angular) {
  angular.module('SimpleServer').controller("HomeController", function ($scope) {
    $scope.msg = "Alooo Home";
  });
})(angular);

(function (angular) {
  angular.module('SimpleServer').controller("WishlistControllerOld", function ($scope) {

    $scope.myInterval = 3000;
    $scope.noWrapSlides = false;
    $scope.activeSlide = 0;

    $scope.parseIdea = function (imgSrc) {
      if (m = (imgSrc || '').match(/^.*\/(.*)\..*$/)) {
        return m[1];
      }
      return imgSrc;
    };

    $scope.slides = [
      {
        image: './app/resources/images/001_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/001_02.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/001_03.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/001_04.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/001_05.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/001_06.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/001_07.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/001_08.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/001_09.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/001_10.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/001_11.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/002_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/002_02.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/002_03.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/003_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/003_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/003_03.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/003_04.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/003_05.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/003_06.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/004_01.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/004_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/004_03.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/005_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/006_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/006_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/006_03.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/006_04.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/006_05.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/007_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/007_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/007_03.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/008_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/008_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/009_01.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/009_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/010_01.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/010_02.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/011_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/011_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/011_03.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/012_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/012_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/012_03.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/012_04.jpeg', 
        available: false,
      },
      {
        image: './app/resources/images/013_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/013_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_01.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_02.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_03.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_04.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_05.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_06.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_07.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_08.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_09.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_10.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_11.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_12.jpeg', 
        available: true,
      },
      {
        image: './app/resources/images/099_13.jpeg', 
        available: true,
      },
    ];
  });
})(angular);

(function (angular) {
  angular.module('SimpleServer').controller("ShopController", function ($scope, $routeParams, $http, $uibModal) {
    $scope.msg = $routeParams.idx;

    $http.get('app/resources/config/shop.json').then((data) => {  
      $scope.itemIdx = $routeParams.idx || 0;
      $scope.items = data.data.items;
    });

    $scope.buy = function () {
      $uibModal.open({
        animation: false,
        templateUrl: 'buy.html',
        size: 'sm'
      });
    };

  });
})(angular);

(function (angular) {
  angular.module('SimpleServer').controller("WishlistController", function ($scope, $http) {
  
    $scope.imagePath = "app/resources/images/wishlist/"
    $http.get('app/resources/config/wishlist.json').then((data) => {
      $scope.items = data.data.items;
    });

    $scope.parseItem = function (input) {
      return input.substr(0, input.lastIndexOf('.')) || input;
    };

  });
})(angular);


(function (angular) {
  angular.module('SimpleServer').controller("TestController", function ($scope) {
    $scope.msg = "Alooo Test";
  });
})(angular);

